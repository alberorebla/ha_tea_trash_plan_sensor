from lxml import html
from datetime import datetime, timedelta
import requests
import dateparser
from .localization_const import DOW, MOY


def get_calendar(city, zone, month, year):
    calendar = []
    cookies = {'selectComune': city, 'selectZona': str(zone)}
    page = requests.get('https://mantovaambiente.it/comuni/%s/start:%s-%s' % (city, year, month), cookies=cookies)
    tree = html.fromstring(page.content)

    cal_zone = tree.xpath('//ul[@class="inner-cal__list" and @data-zona="%s"]' % (zone))[0]
    for element in cal_zone.xpath('div[@class="inner-cal__item"]'):
        date = element.xpath('span[@class="inner-cal__day"]/text()')
        mat = element.xpath('ul[@class="inner-cal__materiali"]/li/text()')
        parsed_date = dateparser.parse(date[0][3:], date_formats=['%d %B'], languages=['it'])
        parsed_date = parsed_date.replace(year=year)
        calendar.append((parsed_date.date(), mat))
    return calendar


def get_next_day_per_material(city, zone, date=datetime.now()):
    list_of_dates = dict()
    dates_per_material = dict()
    next_date = date + timedelta(weeks=2)
    two_month_calendar = get_calendar(city, zone, date.month, date.year)
    if not next_date.month == date.month:
        two_month_calendar = two_month_calendar + get_calendar(city, zone, next_date.month, next_date.year)
    for element in two_month_calendar:
        for material in element[1]:
            dates_per_material.setdefault(material, list())
            dates_per_material.get(material).append(element[0])
    for material, date_list in dates_per_material.items():
        list_of_dates.setdefault(material, min(date_list))
    return list_of_dates


def get_parsed_date(target_date):
    today = (
        datetime.now().date()
    )
    if target_date is not None:
        days_left = (target_date - today).days
        if days_left == 1:
            return 'Domani'
        elif days_left == 0:
            return 'Oggi'
        else:
            return "%s %s %s" % (DOW.get('it_IT').get(target_date.strftime("%A").lower()), target_date.strftime("%d"), MOY.get('it_IT').get(target_date.strftime("%B").lower()))
