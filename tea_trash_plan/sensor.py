"""Platform for sensor integration."""
from datetime import timedelta, datetime
from homeassistant.const import CONF_ADDRESS, CONF_API_KEY
from homeassistant.helpers.entity import Entity
from homeassistant.util import Throttle
import locale

from .tea_api import get_next_day_per_material, get_parsed_date

REQUIREMENTS = ['lxml']
SENSOR_PREFIX = 'Rifiuti '
TRASH_TYPES = [
    dict(name='Plastica', icon='mdi:spray-bottle'),
    dict(name='Organico',icon='mdi:food-apple'),
    dict(name='Carta',icon='mdi:file'),
    dict(name='Sfalci',icon='mdi:tree'),
    dict(name='Vetro',icon='mdi:bottle-wine'),
    dict(name='Indifferenziato', icon='mdi:trash-can')
    ]
MIN_TIME_BETWEEN_UPDATES = timedelta(hours=1)

def setup_platform(hass, config, add_devices, discovery_info=None):
    # """Setup the sensor platform."""
    city = config.get(CONF_ADDRESS)
    tea_code = config.get(CONF_API_KEY)
    data = TrashCollectionSchedule(city, tea_code)

    devices = []
    for trash_type in TRASH_TYPES:
        devices.append(TrashCollectionSensor(trash_type, data))
    add_devices(devices)


class TrashCollectionSensor(Entity):
    """Representation of a Sensor."""

    def __init__(self, trash_type, data):
        """Initialize the sensor."""
        self._state = None
        self._name = trash_type.get('name')
        self._icon = trash_type.get('icon')
        self.data = data
        locale.setlocale(locale.LC_ALL, 'en_EN')

    @property
    def name(self):
        """Return the name of the sensor."""
        return SENSOR_PREFIX + self._name

    @property
    def state(self):
        """Return the state of the sensor."""
        return self._state

    @property
    def icon(self):
        return self._icon

    def update(self):
        """Fetch new state data for the sensor.
        This is the only method that should fetch new data for Home Assistant.
        """
        self.data.update()
        self._state=get_parsed_date(self.data.data.get(self._name))


class TrashCollectionSchedule(object):

    def __init__(self, city, tea_code):
        self._city = city
        self._tea_code = tea_code
        self.data = None

    @Throttle(MIN_TIME_BETWEEN_UPDATES)
    def update(self):
        self.data = get_next_day_per_material(self._city, self._tea_code, datetime.now())
