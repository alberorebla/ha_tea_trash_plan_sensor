# Day of the Week
DOW = dict(
    it_IT=dict(
        monday="Lunedi'"
        , tuesday="Martedi'"
        , wednesday="Mercoledi'"
        , thursday="Giovedi'"
        , friday="Venerdi'"
        , saturday="Sabato'"
        , sunday="Domenica'"
    ),
    en_EN=dict(
        monday='Monday'
        , tuesday='Tuesday'
        , wednesday='Wednesday'
        , thursday='Thursday'
        , friday='Friday'
        , saturday='Saturday'
        , sunday='Sunday'
    )
)

# Month of the Year
MOY = dict(
    it_IT=dict(
        january='Gennaio'
        , february='Febbraio'
        , march='Marzo'
        , april='Aprile'
        , may='Maggio'
        , june='Giugno'
        , july='Luglio'
        , august='Agosto'
        , september='Settembre'
        , october='Ottobre'
        , november='Novembre'
        , december='Dicembre'
    ),
    en_EN=dict(
        january='January'
        , february='February'
        , march='March'
        , april='April'
        , may='May'
        , june='June'
        , july='July'
        , august='August'
        , september='September'
        , october='October'
        , november='November'
        , december='December'
    )
)
