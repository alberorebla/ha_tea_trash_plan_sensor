from datetime import datetime, timedelta
from unittest import TestCase


class TestGet_parsed_date(TestCase):
    def test_get_parsed_date_Today(self):
        from tea_trash_plan.tea_api import get_parsed_date
        self.assertEqual('Oggi',get_parsed_date(datetime.now().date()))

    def test_get_parsed_date_Tomorrow(self):
        from tea_trash_plan.tea_api import get_parsed_date
        self.assertEqual('Domani',get_parsed_date(datetime.now().date() + timedelta(days=1)))

    def test_get_parsed_date_NextDate(self):
        from tea_trash_plan.tea_api import get_parsed_date
        self.assertEqual(datetime.now().date() + timedelta(days=2), get_parsed_date(datetime.now().date() + timedelta(days=2)))

    def test_get_next_day_per_material(self):
        from tea_trash_plan.tea_api import get_next_day_per_material
        print get_next_day_per_material('suzzara', 3696)
