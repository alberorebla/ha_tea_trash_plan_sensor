# Home Assistant / Tea Trash Collector Plan Sensor

Sensor to fetch dates for trash collection dates

Copy ```tea_trash_plan``` folder to ```<config_dir>/custom_components/example_sensor/```.

Before proceed with the configuration go to the tea site <https://www.mantovaambiente.it>

Select you city and go to the complete calendar.
Check now the url in you browser. It should be changed like that:

```https://www.mantovaambiente.it/comuni/<YOURCITY>#calendario```

take note of the ```<YOURCITY>``` value this will be our ```address```. Usually is the name of the city without capitol letter.

Now it comes a bit of hack, you need to open the Development tools of your browser and check
the options available in the selector. Run this script in the console and you will get the list of zones available.

```document.querySelector("#load-calendario-inner > div.inner-cal-toolbar > div.inner-cal-select.inner-cal-select--zone > div > select")```

Now take note of the value of your Zone. This will be our ```api_key```

Configure your sensor in ```configuration.yaml```:
```
sensor:
-  platform: tea_trash_plan
   address: $address
   api_key: $api_code
```

Now you will get all the sensor available. One for kind of waste.
From this time on each sensor will show the next data in which it will be collected by the Tea operator.
